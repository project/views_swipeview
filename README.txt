Views SwipeView
===============
Create slideshows using the jQuery plugin "SwipeView"
(http://cubiq.org/swipeview).

Download the plugin from here: http://cubiq.org/swipeview and place it in your
libraries folder so it looks like "sites/all/libraries/swipeview/swipeview.js".
